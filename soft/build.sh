#!/bin/bash
OPENPATH="/opt/opengrok"
APATCHPATH="/opt/apache"

opengrok(){
    cd ${CURPATH}/soft
    #wget https://gitee.com/rootkie/book/raw/master/soft/opengrok-1.7.23.tar.gz

    mkdir -p ${OPENPATH}
    sudo mkdir -p ${OPENPATH}/{src,data,dist,etc,log}
    sudo chmod 777 ${OPENPATH}
    
    tar -C ${OPENPATH}/dist --strip-components=1 -xzf ${CURPATH}/soft/opengrok-1.7.23.tar.gz
    cp ${OPENPATH}/dist/doc/logging.properties ${OPENPATH}/etc
    cd ${OPENPATH}/src
    cd ${OPENPATH}/dist/tools
    sudo apt-get install -y python3-venv openjdk-11-jdk
    python3 -m venv env
    . ./env/bin/activate
    pip install  opengrok-tools.tar.gz
    opengrok-deploy -c ${OPENPATH}/etc/configuration.xml \
        ${OPENPATH}/dist/lib/source.war ${APATCHPATH}/webapps

    ${APATCHPATH}/bin/startup.sh

    java \
        -Djava.util.logging.config.file=${OPENPATH}/etc/logging.properties \
        -jar ${OPENPATH}/dist/lib/opengrok.jar \
        -c /usr/local/bin/ctags \
        -s ${OPENPATH}/src -d ${OPENPATH}/data -H -P -S -G \
        -W ${OPENPATH}/etc/configuration.xml -U http://localhost:8080/source
}


apatch(){
    mkdir -p ${APATCHPATH}
    cd ${CURPATH}/soft
    #wget https://gitee.com/rootkie/book/raw/master/soft/apache-tomcat-10.0.27.tar.gz
    tar -C ${APATCHPATH} --strip-components=1 -xzf ${CURPATH}/soft/apache-tomcat-10.0.27.tar.gz
}

ctags(){
    sudo apt install -y autoconf automake

    #mkdir ${CURPATH}/soft
    cd ${CURPATH}/soft
    #wget https://gitee.com/rootkie/book/raw/master/soft/ctags.tar.gz
    tar xf ${CURPATH}/soft/ctags.tar.gz
    cd ${CURPATH}/soft/ctags
    ./autogen.sh
    ./configure
    make
    make install
}
CURPATH=`pwd`
ctags
apatch
opengrok

